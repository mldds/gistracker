'use strict';

module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],
    files: [
    ],
    exclude: [
    ],
    reporters: ['progress'],
    browsers: ['PhantomJS'],
    captureTimeout: 60000,
    singleRun: true
  });
};
