'use strict';

exports.config = {
  specs: ['public/tests/e2e/*.js'],
  capabilities: {
    browserName: 'firefox'
  }
};
