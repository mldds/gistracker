describe('Test the main page of Gis Tracker app', function(){
  it('should have a title', function(){
    browser.get('http://localhost:3000/');

    expect(browser.getTitle()).toEqual('GIS Tracker');
  });
});
