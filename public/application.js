(function(){
  'use strict';

  var mainApplicationModuleName = 'gisTracker';

  var mainApplicationModule = angular.module(mainApplicationModuleName,
    ['ui.layout', 'nemLogging', 'ui-leaflet']);

  mainApplicationModule.config(['$locationProvider',
    function($locationProvider){
      $locationProvider.hashPrefix('!');
    }
  ]);

  if(window.location.hash === '#_=_') window.location.hash = '#!';

  angular.element(document).ready(function(){
    angular.bootstrap(document, [mainApplicationModuleName]);
  });
}());
