(function(){
  'use strict';

  process.env.NODE_ENV = process.env.NODE_ENV || 'development';

  var express = require('./config/express'),
      http = require('http');

  var app = express();

  http.createServer(app).listen(app.get('port'), function(){
    console.log('Server running at http://localhost:' + app.get('port') + '/');
  });

  module.exports = app;
}());
